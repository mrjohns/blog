class Post < ActiveRecord::Base
  has_many :comments , dependent: :destroy  # when post  is deleted ,its commets should be deleted as well
  #data validations
  validates_presence_of :title
  validates_presence_of :body


end
